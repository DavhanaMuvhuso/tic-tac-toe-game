import React, { useState, useEffect } from "react";
import "./App.css";
import Board from "./Board";
import Square from "./Square";
import axios from "axios";

const defaultSquares = () => new Array(9).fill(null);
function isTerminal(squares) {
  let w = {};
  w["x"] = false;
  w["o"] = false;

  // Check rows and columns
  for (let i = 0; i < 3; i++) {
    if (
      squares[i * 3] === squares[i * 3 + 1] &&
      squares[i * 3] === squares[i * 3 + 2] &&
      squares[i * 3] !== null
    ) {
      w[squares[i * 3]] = true;
    }
    if (
      squares[i] === squares[i + 3] &&
      squares[i] === squares[i + 6] &&
      squares[i] !== null
    ) {
      w[squares[i]] = true;
    }
  }

  // Check diagonals
  if (
    (squares[0] === squares[4] &&
      squares[0] === squares[8] &&
      squares[0] !== null) ||
    (squares[2] === squares[4] &&
      squares[2] === squares[6] &&
      squares[2] !== null)
  ) {
    w[squares[4]] = true;
  }

  let nx = squares.filter((x) => x === "x").length;
  let no = squares.filter((o) => o === "o").length;

  if (w["x"] && nx === no + 1) {
    return "x";
  }
  if (w["o"] && nx === no) {
    return "o";
  }

  return squares.every((square) => square) ? "draw" : null;
}

function minimax(squares, depth, isMax) {
    const result = isTerminal(squares);
    if (result !== null) {
      if (result === "x") return -10 + depth;
      if (result === "o") return 10 - depth;
      if (result === "draw") return 0;
    }
  
    if (isMax) {
      let maxEvaluation = -Infinity;
      for (let i = 0; i < squares.length; i++) {
        if (!squares[i]) {
          squares[i] = "o";
          const currEvaluation = minimax(squares, depth + 1, false);
          squares[i] = null;
          maxEvaluation = Math.max(maxEvaluation, currEvaluation);
        }
      }
      return maxEvaluation;
    } else {
      let minEvaluation = Infinity;
      for (let i = 0; i < squares.length; i++) {
        if (!squares[i]) {
          squares[i] = "x";
          const currEvaluation = minimax(squares, depth + 1, true);
          squares[i] = null;
          minEvaluation = Math.min(minEvaluation, currEvaluation);
        }
      }
      return minEvaluation;
    }
  }
  
  function findBestMove(squares) {
    let bestMove = -1;
    let bestValue = -Infinity;
  
    for (let i = 0; i < squares.length; i++) {
      if (!squares[i]) {
        squares[i] = "o";
        const moveValue = minimax(squares, 0, false);
        squares[i] = null;
        
        if (moveValue > bestValue) {
          bestValue = moveValue;
          bestMove = i;
        }
      }
    }
  
    return bestMove;
  }
  

function App() {
  const [squares, setSquares] = useState(defaultSquares());
  const [winner, setWinner] = useState(null);
  const [playerName, setPlayerName] = useState("");
  const [topScores, setTopScores] = useState([]);
  const [loadingScores, setLoadingScores] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetchTopScores();
  }, []);

  useEffect(() => {
    const isComputerTurn =
      squares.filter((square) => square !== null).length % 2 === 1;

    if (isComputerTurn) {
      const move = findBestMove(squares);
      if (move !== -1) {
        let newSquares = [...squares];
        newSquares[move] = "o";
        setSquares(newSquares);
      }
    }

    const result = isTerminal(squares);
    if (result && !winner) {
      setWinner(result);
    }
  }, [squares, winner]);

  function handleSquareClick(index) {
    if (squares[index] || winner) return;

    let newSquares = [...squares];
    newSquares[index] = "x";
    setSquares(newSquares);
  }

  // function resetGame() {
  //     setSquares(defaultSquares());
  //     setWinner(null);
  // }

  const fetchTopScores = async () => {
    try {
      setLoadingScores(true);
      const response = await axios.get("http://localhost:8000/api/top-scores");
      setTopScores(response.data);
      setLoadingScores(false);
    } catch (err) {
      setError("Error fetching top scores.");
      setLoadingScores(false);
    }
  };

  const saveScore = async (name, score) => {
    try {
      await axios.post("http://localhost:8000/api/scores", {
        player_name: name,
        score: score,
      });

      fetchTopScores();
    } catch (err) {
      console.error(
        "Error when saving score:",
        err.response ? err.response.data : err.message
      );
      setError("Error saving score.");
    }
  };

  const resetGame = () => {
    if (winner === "x" && playerName.trim()) {
      saveScore(playerName, 1);
    } else if (winner === "o" && playerName.trim()) {
      saveScore(playerName, 0);
    } else if (winner === "draw" && playerName.trim()) {
      saveScore(playerName, 2); 
    }

    setSquares(defaultSquares());
    setWinner(null);
    setPlayerName("");
  };
  return (
    <main>
      <div className="player-input">
        <label>Enter Player Name:</label>
        <input
          value={playerName}
          onChange={(e) => setPlayerName(e.target.value)}
          placeholder="Player name..."
        />
      </div>
      <Board>
        {squares.map((square, index) => (
          <Square
            x={square === "x" ? 1 : 0}
            o={square === "o" ? 1 : 0}
            onClick={() => handleSquareClick(index)}
            key={index}
          />
        ))}
      </Board>
      {!!winner && winner === "x" && (
        <div className="result green">You WON!</div>
      )}
      {!!winner && winner === "o" && (
        <div className="result red">You LOST!</div>
      )}
      {!!winner && winner === "draw" && (
        <div className="result gray">It's a DRAW!</div>
      )}

      <button className="reset-button" onClick={resetGame}>
        Reset Game
      </button>
      <div className="top-scores">
        <h2>Player Scores</h2>
        {loadingScores ? (
          <p>Loading...</p>
        ) : (
          <ul>
            {topScores.map((score) => (
              <li key={score.id}>
                {score.player_name}: {score.score}
              </li>
            ))}
          </ul>
        )}
        {error && <p className="error-message">{error}</p>}
      </div>
    </main>
  );
}

export default App;
