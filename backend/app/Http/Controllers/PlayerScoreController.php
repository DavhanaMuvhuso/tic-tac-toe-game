<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PlayerScore;  

class PlayerScoreController extends Controller

    public function store(Request $request)
    {
        $request->validate([
            'player_name' => 'required|string|max:255',
            'score' => 'required|integer',
        ]);

        $score = new PlayerScore();
        $score->player_name = $request->player_name;
        $score->score = $request->score;
        $score->save();

        return response()->json(['message' => 'Score saved successfully!', 'data' => $score], 201);
    }


    public function topScores()
    {
        $scores = PlayerScore::orderBy('score', 'desc')->take(10)->get();
        return response()->json($scores);
    }
}
